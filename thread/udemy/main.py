import threading
from threading import Thread

t = threading.current_thread().getName()

print(t) # MainThread

# Check if thread is main thread

if threading.current_thread() == threading.main_thread():
    print("This is main thread")
else:
    print("This is not main thread")

# Set name for thread

threading.current_thread().name = 'Diep'

# create a thread
def test():
    print(threading.current_thread().getName())


t = Thread(target=test, name='test_thread')
t.start()

# Create thread extending Thread

class MyThread(Thread):
    def run(self):
        print(threading.current_thread().getName())
obj = MyThread()
obj.start()

# Create thread without extending Thread

class MyThread():
    def test(self):
        print('test')
t = Thread(target=Mythread().test)
t.start()